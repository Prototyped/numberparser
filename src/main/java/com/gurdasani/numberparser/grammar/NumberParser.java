package com.gurdasani.numberparser.grammar;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.TokenStream;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class NumberParser {
    public static void main(String[] args) {
        String argString = Stream.of(args).collect(joining(" "));
        System.out.println(parseNumberWords(argString));
    }

    static BigDecimal parseNumberWords(String numberWordString) {
        CharStream stream = CharStreams.fromString(numberWordString);
        Lexer wordLexer = new NumberWordLexer(stream);
        wordLexer.removeErrorListeners();
        wordLexer.addErrorListener(ThrowingErrorListener.getInstance());
        TokenStream tokens = new CommonTokenStream(wordLexer);
        NumberWordGrammar grammar = new NumberWordGrammar(tokens);
        grammar.removeErrorListeners();
        grammar.addErrorListener(ThrowingErrorListener.getInstance());
        NumberConstructingVisitor visitor = new NumberConstructingVisitor();

        return grammar.spelledOutNumber().accept(visitor);
    }
}
