package com.gurdasani.numberparser.grammar;

import com.google.common.collect.ImmutableMap;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.math.BigDecimal;
import java.util.Map;
import java.util.function.Function;

import static com.google.common.base.Preconditions.checkState;

public final class NumberConstructingVisitor extends NumberWordGrammarBaseVisitor<BigDecimal> {
    private static final Map<Function<NumberWordGrammar.TensTermContext, TerminalNode>, BigDecimal> TENS_TERM_TRANSLATOR
            = ImmutableMap.<Function<NumberWordGrammar.TensTermContext, TerminalNode>, BigDecimal>builder()
                .put(NumberWordGrammar.TensTermContext::TWENTY,     BigDecimal.valueOf(20))
                .put(NumberWordGrammar.TensTermContext::THIRTY,     BigDecimal.valueOf(30))
                .put(NumberWordGrammar.TensTermContext::FORTY,      BigDecimal.valueOf(40))
                .put(NumberWordGrammar.TensTermContext::FIFTY,      BigDecimal.valueOf(50))
                .put(NumberWordGrammar.TensTermContext::SIXTY,      BigDecimal.valueOf(60))
                .put(NumberWordGrammar.TensTermContext::SEVENTY,    BigDecimal.valueOf(70))
                .put(NumberWordGrammar.TensTermContext::EIGHTY,     BigDecimal.valueOf(80))
                .put(NumberWordGrammar.TensTermContext::NINETY,     BigDecimal.valueOf(90))
                .build();

    private static final Map<Function<NumberWordGrammar.TeensTermContext, TerminalNode>, BigDecimal>
            TEENS_TERM_TRANSLATOR
            = ImmutableMap.<Function<NumberWordGrammar.TeensTermContext, TerminalNode>, BigDecimal>builder()
                .put(NumberWordGrammar.TeensTermContext::TEN,       BigDecimal.TEN)
                .put(NumberWordGrammar.TeensTermContext::ELEVEN,    BigDecimal.valueOf(11))
                .put(NumberWordGrammar.TeensTermContext::TWELVE,    BigDecimal.valueOf(12))
                .put(NumberWordGrammar.TeensTermContext::THIRTEEN,  BigDecimal.valueOf(13))
                .put(NumberWordGrammar.TeensTermContext::FOURTEEN,  BigDecimal.valueOf(14))
                .put(NumberWordGrammar.TeensTermContext::FIFTEEN,   BigDecimal.valueOf(15))
                .put(NumberWordGrammar.TeensTermContext::SIXTEEN,   BigDecimal.valueOf(16))
                .put(NumberWordGrammar.TeensTermContext::SEVENTEEN, BigDecimal.valueOf(17))
                .put(NumberWordGrammar.TeensTermContext::EIGHTEEN,  BigDecimal.valueOf(18))
                .put(NumberWordGrammar.TeensTermContext::NINETEEN,  BigDecimal.valueOf(19))
                .build();

    private static final Map<Function<NumberWordGrammar.UnitsTermContext, TerminalNode>, BigDecimal>
            UNITS_TERM_TRANSLATOR
            = ImmutableMap.<Function<NumberWordGrammar.UnitsTermContext, TerminalNode>, BigDecimal>builder()
                .put(NumberWordGrammar.UnitsTermContext::ZERO,      BigDecimal.ZERO)
                .put(NumberWordGrammar.UnitsTermContext::ONE,       BigDecimal.ONE)
                .put(NumberWordGrammar.UnitsTermContext::TWO,       BigDecimal.valueOf(2))
                .put(NumberWordGrammar.UnitsTermContext::THREE,     BigDecimal.valueOf(3))
                .put(NumberWordGrammar.UnitsTermContext::FOUR,      BigDecimal.valueOf(4))
                .put(NumberWordGrammar.UnitsTermContext::FIVE,      BigDecimal.valueOf(5))
                .put(NumberWordGrammar.UnitsTermContext::SIX,       BigDecimal.valueOf(6))
                .put(NumberWordGrammar.UnitsTermContext::SEVEN,     BigDecimal.valueOf(7))
                .put(NumberWordGrammar.UnitsTermContext::EIGHT,     BigDecimal.valueOf(8))
                .put(NumberWordGrammar.UnitsTermContext::NINE,      BigDecimal.valueOf(9))
                .build();

    @Override
    public BigDecimal visitDecillionsNumber(NumberWordGrammar.DecillionsNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.DECILLION(),      33, ctx.preNum);
    }

    @Override
    public BigDecimal visitNonillionsNumber(NumberWordGrammar.NonillionsNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.NONILLION(),      30, ctx.preNum);
    }

    @Override
    public BigDecimal visitOctillionsNumber(NumberWordGrammar.OctillionsNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.OCTILLION(),      27, ctx.preNum);
    }

    @Override
    public BigDecimal visitSeptillionsNumber(NumberWordGrammar.SeptillionsNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.SEPTILLION(),     24, ctx.preNum);
    }

    @Override
    public BigDecimal visitSextillionsNumber(NumberWordGrammar.SextillionsNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.SEXTILLION(),     21, ctx.preNum);
    }

    @Override
    public BigDecimal visitQuintillionsNumber(NumberWordGrammar.QuintillionsNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.QUINTILLION(),    18, ctx.preNum);
    }

    @Override
    public BigDecimal visitShankhsNumber(NumberWordGrammar.ShankhsNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.SHANKH(),         17, ctx.preNum);
    }

    @Override
    public BigDecimal visitPadmasNumber(NumberWordGrammar.PadmasNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.PADMA(),          15, ctx.preNum);
    }

    @Override
    public BigDecimal visitQuadrillionsNumber(NumberWordGrammar.QuadrillionsNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.QUADRILLION(),    15, ctx.preNum);
    }

    @Override
    public BigDecimal visitNeelsNumber(NumberWordGrammar.NeelsNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.NEEL(),           13, ctx.preNum);
    }

    @Override
    public BigDecimal visitTrillionsNumber(NumberWordGrammar.TrillionsNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.TRILLION(),       12, ctx.preNum);
    }

    @Override
    public BigDecimal visitKharabsNumber(NumberWordGrammar.KharabsNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.KHARAB(),         11, ctx.preNum);
    }

    @Override
    public BigDecimal visitArabsNumber(NumberWordGrammar.ArabsNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.ARAB(),            9, ctx.preNum);
    }

    @Override
    public BigDecimal visitBillionsNumber(NumberWordGrammar.BillionsNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.BILLION(),         9, ctx.preNum);
    }

    @Override
    public BigDecimal visitCroresNumber(NumberWordGrammar.CroresNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.CRORE(),           7, ctx.preNum);
    }

    @Override
    public BigDecimal visitMillionsNumber(NumberWordGrammar.MillionsNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.MILLION(),         6, ctx.preNum);
    }

    @Override
    public BigDecimal visitLakhsNumber(NumberWordGrammar.LakhsNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.LAKH(),            5, ctx.preNum);
    }

    @Override
    public BigDecimal visitThousandsNumber(NumberWordGrammar.ThousandsNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.THOUSAND(),        3, ctx.preNum);
    }

    @Override
    public BigDecimal visitHundredsNumber(NumberWordGrammar.HundredsNumberContext ctx) {
        return levelDown(ctx, ctx.subNum, ctx.postNum, ctx.HUNDRED(),         2, ctx.numerals, ctx.preNum);
    }

    @Override
    public BigDecimal visitTensNumber(NumberWordGrammar.TensNumberContext ctx) {
        if (ctx.numerals != null) {
            return ctx.numerals.accept(this);
        } else if (ctx.units != null) {
            return ctx.units.accept(this);
        } else if (ctx.teens != null) {
            return ctx.teens.accept(this);
        } else {
            BigDecimal num = ctx.tens.accept(this);
            if (ctx.postNum != null) {
                num = num.add(ctx.postNum.accept(this));
            }

            return num;
        }
    }

    @Override
    public BigDecimal visitTensTerm(NumberWordGrammar.TensTermContext ctx) {
        return selectTerm(ctx, TENS_TERM_TRANSLATOR,  "tens");
    }

    @Override
    public BigDecimal visitTeensTerm(NumberWordGrammar.TeensTermContext ctx) {
        return selectTerm(ctx, TEENS_TERM_TRANSLATOR, "teens");
    }

    @Override
    public BigDecimal visitUnitsTerm(NumberWordGrammar.UnitsTermContext ctx) {
        return selectTerm(ctx, UNITS_TERM_TRANSLATOR, "units");
    }

    @Override
    public BigDecimal visitNumber(NumberWordGrammar.NumberContext ctx) {
        BigDecimal num = super.visitNumber(ctx);
        if (ctx.HYPHEN() != null) {
            num = num.negate();
        }
        return num;
    }

    @Override
    public BigDecimal visitAnglo_grouped(NumberWordGrammar.Anglo_groupedContext ctx) {
        return visitGroupedDigits(ctx, NumberWordGrammar.POINT);
    }

    @Override
    public BigDecimal visitFrench_grouped(NumberWordGrammar.French_groupedContext ctx) {
        return visitGroupedDigits(ctx, NumberWordGrammar.COMMA);
    }

    @Override
    public BigDecimal visitNorwegian_grouped(NumberWordGrammar.Norwegian_groupedContext ctx) {
        return visitGroupedDigits(ctx, NumberWordGrammar.COMMA);
    }

    @Override
    public BigDecimal visitGerman_grouped(NumberWordGrammar.German_groupedContext ctx) {
        return visitGroupedDigits(ctx, NumberWordGrammar.COMMA);
    }

    private BigDecimal levelDown(ParserRuleContext ctx, ParserRuleContext subNum, ParserRuleContext postNum,
                                 TerminalNode keyword, int powerOfTen, ParserRuleContext... preNum) {
        if (keyword != null) {
            BigDecimal num = parsePreNum(preNum);

            num = num.movePointRight(powerOfTen);
            num = addPostNum(num, postNum);

            return num;
        } else {
            checkState(subNum != null,
                    "Expected a number quantity subordinate to %s: %s",
                    ctx.getClass().getName(), ctx.getText());
            return subNum.accept(this);
        }

    }

    private BigDecimal parsePreNum(ParserRuleContext... preNum) {
        for (ParserRuleContext num : preNum) {
            if (num != null) {
                return num.accept(this);
            }
        }
        return BigDecimal.ONE;
    }

    private BigDecimal addPostNum(BigDecimal num, ParserRuleContext postNum) {
        if (postNum != null) {
            BigDecimal postNumResult = postNum.accept(this);
            num = addToMagnitude(num, postNumResult);
        }
        return num;
    }

    private <TERMCTX extends ParserRuleContext> BigDecimal selectTerm(
            TERMCTX ctx, Map<Function<TERMCTX, TerminalNode>, BigDecimal> termTranslator, String termType) {
        for (Map.Entry<Function<TERMCTX, TerminalNode>, BigDecimal> entry
                : termTranslator.entrySet()) {
            if (entry.getKey().apply(ctx) != null) {
                return entry.getValue();
            }
        }
        throw new IllegalStateException("Unrecognized " + termType + " term with text " + ctx.getText());
    }

    private BigDecimal visitGroupedDigits(ParserRuleContext ctx, int pointTokenType) {
        boolean pointEncountered = false;
        BigDecimal num = BigDecimal.ZERO;
        for (ParseTree child : ctx.children) {
            if (isDigits(child)) {
                String digitsText = child.getText();
                BigDecimal augend = new BigDecimal(digitsText);
                if (!pointEncountered) {
                    num = num.movePointRight(digitsText.length());
                } else {
                    augend = augend.movePointLeft(digitsText.length());
                }
                num = addToMagnitude(num, augend);
            } else if (isTokenOfType(child, pointTokenType)) {
                pointEncountered = true;
            }
        }

        return num;
    }

    private BigDecimal addToMagnitude(BigDecimal num, BigDecimal augend) {
        if (isNonNegative(num)) {
            num = num.add(augend);
        } else {
            num = num.subtract(augend);
        }
        return num;
    }

    private boolean isNonNegative(BigDecimal num) {
        return BigDecimal.ZERO.compareTo(num) <= 0;
    }

    private boolean isDigits(ParseTree child) {
        return isTokenOfType(child, NumberWordGrammar.DIGITS);
    }

    private boolean isTokenOfType(ParseTree child, int tokenType) {
        return child instanceof TerminalNode
                && ((TerminalNode) child).getSymbol().getType() == tokenType;
    }
}
