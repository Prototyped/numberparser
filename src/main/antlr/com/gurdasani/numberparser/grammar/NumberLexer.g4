lexer grammar NumberLexer;

fragment DIGIT              : [0-9] ;
HYPHEN                      : '-' ;
PLUS                        : '+' ;
DIGITS                      : DIGIT+ ;
COMMA                       : ',' ;
POINT                       : '.' ;
SPACE                       : ' ' ;
