parser grammar NumberGrammar;

options {
    tokenVocab = NumberLexer;
}

number              : (HYPHEN | PLUS)?
                      (anglo_grouped | french_grouped | norwegian_grouped | german_grouped)
                    ;

anglo_grouped       : (DIGITS COMMA)* DIGITS (POINT DIGITS)?
                    ;

french_grouped      : (DIGITS SPACE)* DIGITS (COMMA DIGITS)?
                    ;

norwegian_grouped   : (DIGITS POINT)* DIGITS (COMMA DIGITS)?
                    ;

german_grouped      : ((DIGITS SPACE)* DIGITS POINT)? DIGITS (COMMA DIGITS)?
                    ;