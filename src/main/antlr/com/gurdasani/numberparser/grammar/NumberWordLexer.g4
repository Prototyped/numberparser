lexer grammar NumberWordLexer;

import NumberLexer;

ZERO            : 'zero' ;
ONE             : 'one' ;
TWO             : 'two' ;
THREE           : 'three' ;
FOUR            : 'four' ;
FIVE            : 'five' ;
SIX             : 'six' ;
SEVEN           : 'seven' ;
EIGHT           : 'eight' ;
NINE            : 'nine' ;

TEN             : 'ten' ;
ELEVEN          : 'eleven' ;
TWELVE          : 'twelve' ;
THIRTEEN        : 'thirteen' ;
FOURTEEN        : 'fourteen' ;
FIFTEEN         : 'fifteen' ;
SIXTEEN         : 'sixteen' ;
SEVENTEEN       : 'seventeen' ;
EIGHTEEN        : 'eighteen' ;
NINETEEN        : 'nineteen' ;

TWENTY          : 'twenty' ;
THIRTY          : 'thirty' ;
FORTY           : 'forty' ;
FIFTY           : 'fifty' ;
SIXTY           : 'sixty' ;
SEVENTY         : 'seventy' ;
EIGHTY          : 'eighty' ;
NINETY          : 'ninety' ;

HUNDRED         : 'hundred' ;
THOUSAND        : 'thousand' ;
LAKH            : ('lakh'|'lac');
MILLION         : 'million' ;
CRORE           : 'crore' ;
BILLION         : ('billion'|'milliard') ;
ARAB            : 'arab';
KHARAB          : 'kharab' ;
TRILLION        : 'trillion' ;
NEEL            : 'neel' ;
QUADRILLION     : ('quadrillion'|'billiard') ;
PADMA           : 'padma' ;
SHANKH          : 'shankh' ;
QUINTILLION     : 'quintillion' ;
SEXTILLION      : 'sextillion' ;
SEPTILLION      : 'septillion' ;
OCTILLION       : 'octillion' ;
NONILLION       : 'nonillion' ;
DECILLION       : 'decillion' ;

S               : 's' ;
A               : 'a' ;

SPACE           : ' ' ;

CONJUNCTION     : ('and' | '&') ;
