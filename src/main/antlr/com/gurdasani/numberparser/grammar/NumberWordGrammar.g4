parser grammar NumberWordGrammar;

import NumberGrammar;

options {
    tokenVocab = NumberWordLexer;
}

spelledOutNumber    : decillionsNumber
                    | shankhsNumber
                    ;

decillionsNumber    : subNum=nonillionsNumber
                    | ((A | preNum=nonillionsNumber) SPACE)?
                      DECILLION
                      (SPACE (CONJUNCTION SPACE)? postNum=nonillionsNumber)?
                    ;

nonillionsNumber    : subNum=octillionsNumber
                    | ((A | preNum=octillionsNumber) SPACE)?
                      NONILLION
                      (SPACE (CONJUNCTION SPACE)? postNum=octillionsNumber)?
                    ;

octillionsNumber    : subNum=septillionsNumber
                    | ((A | preNum=septillionsNumber) SPACE)?
                      OCTILLION
                      (SPACE (CONJUNCTION SPACE)? postNum=septillionsNumber)?
                    ;

septillionsNumber   : subNum=sextillionsNumber
                    | ((A | preNum=sextillionsNumber) SPACE)?
                      SEPTILLION
                      (SPACE (CONJUNCTION SPACE)? postNum=sextillionsNumber)?
                    ;

sextillionsNumber   : subNum=quintillionsNumber
                    | ((A | preNum=quintillionsNumber) SPACE)?
                      SEXTILLION
                      (SPACE (CONJUNCTION SPACE)? postNum=quintillionsNumber)?
                    ;

shankhsNumber       : subNum=padmasNumber
                    | ((A | preNum=padmasNumber) SPACE)?
                      SHANKH
                      (SPACE (CONJUNCTION SPACE)? postNum=padmasNumber)?
                    ;

quintillionsNumber  : subNum=quadrillionsNumber
                    | ((A | preNum=quadrillionsNumber) SPACE)?
                      QUINTILLION
                      (SPACE (CONJUNCTION SPACE)? postNum=quadrillionsNumber)?
                    ;

padmasNumber        : subNum=neelsNumber
                    | ((A | preNum=neelsNumber) SPACE)?
                      PADMA
                      (SPACE (CONJUNCTION SPACE)? postNum=neelsNumber)?
                    ;

quadrillionsNumber  : subNum=trillionsNumber
                    | ((A | preNum=trillionsNumber) SPACE)?
                      QUADRILLION
                      (SPACE (CONJUNCTION SPACE)? postNum=trillionsNumber)?
                    ;

neelsNumber         : subNum=kharabsNumber
                    | ((A | preNum=kharabsNumber) SPACE)?
                      NEEL
                      (SPACE (CONJUNCTION SPACE)? postNum=kharabsNumber)?
                    ;

trillionsNumber     : subNum=billionsNumber
                    | ((A | preNum=billionsNumber) SPACE)?
                      TRILLION
                      (SPACE (CONJUNCTION SPACE)? postNum=billionsNumber)?
                    ;

kharabsNumber       : subNum=arabsNumber
                    | ((A | preNum=arabsNumber) SPACE)?
                      KHARAB
                      (SPACE (CONJUNCTION SPACE)? postNum=arabsNumber)?
                    ;

arabsNumber         : subNum=croresNumber
                    | ((A | preNum=croresNumber) SPACE)?
                      ARAB
                      (SPACE (CONJUNCTION SPACE)? postNum=croresNumber)?
                    ;

billionsNumber      : subNum=millionsNumber
                    | ((A | preNum=hundredsNumber) SPACE)?
                      BILLION
                      (SPACE (CONJUNCTION SPACE)? postNum=millionsNumber)?
                    ;

croresNumber        : subNum=lakhsNumber
                    | ((A | preNum=lakhsNumber) SPACE)?
                      CRORE
                      (SPACE (CONJUNCTION SPACE)? postNum=lakhsNumber)?
                    ;

millionsNumber      : subNum=thousandsNumber
                    | ((A | preNum=hundredsNumber) SPACE)?
                      MILLION
                      (SPACE (CONJUNCTION SPACE)? postNum=thousandsNumber)?
                    ;

lakhsNumber         : subNum=thousandsNumber
                    | ((A | preNum=thousandsNumber) SPACE)?
                      LAKH
                      (SPACE (CONJUNCTION SPACE)? postNum=thousandsNumber)?
                    ;

thousandsNumber     : subNum=hundredsNumber
                    | ((A | preNum=hundredsNumber) SPACE)?
                      THOUSAND
                      (SPACE (CONJUNCTION SPACE)? postNum=hundredsNumber)?
                    ;

hundredsNumber      : subNum=tensNumber
                    | ((A | preNum=unitsTerm | numerals=number) SPACE)?
                      HUNDRED
                      (SPACE (CONJUNCTION SPACE)? postNum=tensNumber)?
                    ;

tensNumber          : numerals=number
                    | units=unitsTerm
                    | teens=teensTerm
                    | tens=tensTerm
                      ((SPACE | HYPHEN) postNum=unitsTerm)?
                    ;

tensTerm            : TWENTY
                    | THIRTY
                    | FORTY
                    | FIFTY
                    | SIXTY
                    | SEVENTY
                    | EIGHTY
                    | NINETY
                    ;

teensTerm           : TEN
                    | ELEVEN
                    | TWELVE
                    | THIRTEEN
                    | FOURTEEN
                    | FIFTEEN
                    | SIXTEEN
                    | SEVENTEEN
                    | EIGHTEEN
                    | NINETEEN
                    ;

unitsTerm           : ZERO
                    | ONE
                    | TWO
                    | THREE
                    | FOUR
                    | FIVE
                    | SIX
                    | SEVEN
                    | EIGHT
                    | NINE
                    ;
