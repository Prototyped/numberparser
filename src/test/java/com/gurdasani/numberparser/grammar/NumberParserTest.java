package com.gurdasani.numberparser.grammar;

import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.Timeout;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertEquals;

public class NumberParserTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Rule
    public Timeout timeout = Timeout.seconds(10L);

    @Test
    public void longStringText() {
        BigDecimal expected = new BigDecimal("2566789898.923")
                .movePointRight(17)
                .add(new BigDecimal("60226589464829652"));
        String number = "2 566 789.898,923 hundred sixty quadrillion two hundred twenty six trillion five hundred eighty nine billion four hundred sixty four million eight hundred twenty nine thousand six hundred and fifty two";

        BigDecimal result = NumberParser.parseNumberWords(number);

        assertEquals(expected, result);
    }

    @Test
    public void longGermanNumber() {
        BigDecimal expected = new BigDecimal("-2566789898.923");
        String number = "-2 566 789.898,923";

        BigDecimal result = NumberParser.parseNumberWords(number);

        assertEquals(expected, result);

    }

    @Test
    public void negativeStringTest() {
        BigDecimal expected = new BigDecimal("-1023");
        String number = "-1 thousand twenty three";

        BigDecimal result = NumberParser.parseNumberWords(number);

        assertEquals(expected, result);
    }

    @Test
    public void quadrillions() {
        Number number = NumberParser.parseNumberWords("two hundred sixty quadrillion two hundred twenty-six trillion five hundred eighty-nine billion four hundred sixty-four million eight hundred twenty-nine thousand six hundred fifty-two");

        assertEquals(BigDecimal.valueOf(260_226_589_464_829_652L), number);
    }

    @Test
    public void sanerText() {
        Number number = NumberParser.parseNumberWords("26.5 million");

        assertEquals(BigDecimal.valueOf(26_500_000L), number);
    }

    @Test
    public void fiveHundredAndSixtyTwo() {
        Number number = NumberParser.parseNumberWords("five hundred and sixty-two");

        assertEquals(BigDecimal.valueOf(562L), number);
    }

    @Test
    public void crores() {
        Number number = NumberParser.parseNumberWords("six crore twenty-nine lakh fifty-three thousand two hundred eighty-six");

        assertEquals(BigDecimal.valueOf(6_29_53_286L), number);
    }

    @Test
    public void decillions() {
        // given
        String words = "two million eight hundred sixteen thousand four hundred ninety eight decillion "
                + "six hundred thirty-three nonillion "
                + "seven hundred six octillion "
                + "fifty seven septillion "
                + "eight hundred ten sextillion "
                + "one hundred sixty three quintillion "
                + "nine hundred ninety nine quadrillion "
                + "one hundred twenty three trillion "
                + "four hundred forty two billion "
                + "sixty eight million "
                + "three hundred twenty thousand "
                + "five hundred and two";

        BigDecimal expected = new BigDecimal("2816498" + "633" + "706" + "057" + "810" + "163" + "999" + "123"
                                             + "442" + "068" + "320" + "502");

        // when
        Number number = NumberParser.parseNumberWords(words);

        // then
        assertEquals(expected, number);
    }

    @Test
    public void shankhs() {
        // given
        String words = "twenty-six crore fifty-eight lac forty-five thousand seven hundred sixty-three shankh "
                + "fifty-three padma "
                + "twenty-six neel "
                + "eighty-nine kharab "
                + "forty-two arab "
                + "thirty crore "
                + "six lakh "
                + "seventy-five thousand "
                + "three hundred twenty one";

        BigDecimal expected = new BigDecimal("265845763" + "53" + "26" + "89" + "42" + "30" + "06" + "75" + "321");

        // when
        Number number = NumberParser.parseNumberWords(words);

        // then
        assertEquals(expected, number);
    }

    @Test
    public void skippedScale() {
        // given
        String words = "twenty million three hundred and five";

        BigDecimal expected = BigDecimal.valueOf(20_000_305L);

        // when
        Number number = NumberParser.parseNumberWords(words);

        // then
        assertEquals(expected, number);
    }

    @Test
    public void aHundred() {
        // given
        String aHundred = "a hundred";
        BigDecimal expected = BigDecimal.valueOf(100L);

        // when
        Number number = NumberParser.parseNumberWords(aHundred);

        // then
        assertEquals(expected, number);
    }

    @Test
    public void yearRanges() {
        // given
        String ranges = "1960 1960 1961 1963 1964 1967 1968 1972 1975 1975–76 1976–77 1978 1979 1979";

        // then
        thrown.expect(ParseCancellationException.class);
        thrown.expectMessage("token recognition error at: '–'");

        // when
        NumberParser.parseNumberWords(ranges);
    }

    @Test
    public void ridiculousRegexpPassingString() {
        // given
        String ridiculous = "a thirty 30 million thirty 30 a";

        // then
        thrown.expect(ParseCancellationException.class);
        thrown.expectMessage("no viable alternative at input 'a thirty'");

        // when
        NumberParser.parseNumberWords(ridiculous);
    }

}
